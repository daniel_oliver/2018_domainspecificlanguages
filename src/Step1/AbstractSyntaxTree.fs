﻿module Step2.AbstractSyntaxTree
/// Recursive tree
type Tree =
    | TextFragment of Text: string
    | VariableFragment of VariableName: string
    | Root of Tree list

module Example =
    /// Hello, {@firstName}, good evening!
    let goodEveningTree =
        Tree.Root
            [   (Tree.TextFragment "Hello, ")
                (Tree.VariableFragment "firstName")
                (Tree.TextFragment ", good evening!")
            ]
