﻿module TestCleanParser


open NUnit.Framework
open System
open Step2.AbstractSyntaxTree

[<TestCleanParser>]
type TestCleanParser () =
    
    [<Test>]
    member this.TestCleanParserSucceeds() =
        let sampleText = "Hello, {@firstName}, good evening!"
        let goodEveningTree =
            Tree.Root
                [   (Tree.TextFragment "Hello, ")
                    (Tree.VariableFragment "firstName")
                    (Tree.TextFragment ", good evening!")
                ]

        match 
            sampleText
            |> FLexer.Core.ClassifierStatus<string>.OfString
            |> Step4.CleanParser.ReadTree with
        | Ok (treeResult, status) ->
            Assert.AreEqual(
                goodEveningTree,
                treeResult
                )
        | Error error ->
            Assert.Fail(error.ToString())