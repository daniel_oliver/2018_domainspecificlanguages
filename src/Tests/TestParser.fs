﻿module TestParser


open NUnit.Framework
open Step4
open System
open Step2.AbstractSyntaxTree

[<TestParser>]
type TestParser () =
    
    [<Test>]
    member this.TestReadTextFragmentSucceeds() =
        let sampleText = "Until Curly Bracket {"

        match Parser.ReadTextFragment sampleText with
        | Ok (textFragment, remainderText) ->
            Assert.AreEqual(
                Step2.AbstractSyntaxTree.Tree.TextFragment "Until Curly Bracket ", 
                textFragment)
            Assert.AreEqual(
                "{",
                remainderText)
        | Error error ->
            Assert.Fail(error)
            
    [<Test>]
    member this.TestReadVariableSucceeds() =
        let sampleText = "{@firstName}"

        match Parser.ReadVariable sampleText with
        | Ok (textFragment, remainderText) ->
            Assert.AreEqual(
                Step2.AbstractSyntaxTree.Tree.VariableFragment "firstName", 
                textFragment)
            Assert.AreEqual(
                String.Empty,
                remainderText)
        | Error error ->
            Assert.Fail(error)
            
    [<Test>]
    member this.TestReadTextFragmentFails() =
        let sampleText = "{"

        match Parser.ReadTextFragment sampleText with
        | Ok _ ->
            Assert.Fail()
        | Error _ ->
            Assert.Pass()

    [<Test>]
    member this.TestReadVariableFails() =
        let sampleText = "Until Curly Bracket {"

        match Parser.ReadVariable sampleText with
        | Ok _ ->
            Assert.Fail()
        | Error _ ->
            Assert.Pass()

            
    [<Test>]
    member this.TestReadTextTemplateSucceeds() =
        let sampleText = "Hello, {@firstName}, good evening!"
        let goodEveningTree =
            Tree.Root
                [   (Tree.TextFragment "Hello, ")
                    (Tree.VariableFragment "firstName")
                    (Tree.TextFragment ", good evening!")
                ]

        match Parser.ReadTextTemplate sampleText [] with
        | Ok (result, remainderText) ->
            Assert.AreEqual(
                goodEveningTree,
                result
                )
        | Error error ->
            Assert.Fail(error)