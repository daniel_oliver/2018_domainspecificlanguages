namespace Tests.Tests

open NUnit.Framework

[<TestLexer>]
type TestLexer () =
    
    [<Test>]
    member this.TestReadWordSucceeds() =
        let sampleText = "Find word in this string"

        match Lexer.ReadWord "Find" sampleText with
        | Ok result ->
            Assert.AreEqual(
                "Find", 
                result.ReadText)
        | Error error ->
            Assert.Fail(error)
            
    [<Test>]
    member this.TestReadWordFails() =
        let sampleText = "Nothing to Find here"

        match Lexer.ReadWord "Find" sampleText with
        | Ok result ->
            Assert.Fail(
                sprintf "Found match: %s" 
                    result.ReadText)
        | Error error ->
            Assert.Pass()
                        
    [<Test>]
    member this.TestRegexSucceeds() =
        let sampleText = "Find word in this string"

        match Lexer.ReadRegex "[A-Za-z]+" sampleText with
        | Ok result ->
            Assert.AreEqual(
                "Find", 
                result.ReadText)
        | Error error ->
            Assert.Fail(error)

    [<Test>]
    member this.TestRegexFails() =
        let sampleText = "   Find word in this string"

        match Lexer.ReadRegex "[A-Za-z]+" sampleText with
        | Ok result ->
            Assert.Fail(
                sprintf "Found match: %s" 
                    result.ReadText)
        | Error error ->
            Assert.Pass()