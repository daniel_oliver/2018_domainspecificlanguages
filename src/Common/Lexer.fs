module Lexer

open System

type ReadText =
    {   RemainderText: string
        ReadText: string
    }

/// Tries to read one word. 
let ReadWord (word: string) (text: string)
    : Result<ReadText, string> =
    if text.StartsWith(word) then
        let remainder =
            if text.Length > word.Length then 
                text.Substring(word.Length) 
            else String.Empty

        {   ReadText.ReadText = word
            ReadText.RemainderText = remainder
        } 
        |> Result.Ok
    else
        (sprintf "Couldn't find word: %s" word)
        |> Result.Error

/// Tries to match a regex.
let ReadRegex (regexPattern: string) (text: string): Result<ReadText, string> =    
    // Get all Regex matches
    System.Text.RegularExpressions.Regex.Matches(text, regexPattern)
    // Cast Regex MatchCollection to Match
    |> Seq.cast<System.Text.RegularExpressions.Match>
    // Must start at character index 0 of input
    |> Seq.filter(fun regexMatch -> regexMatch.Index = 0)
    // Find the longest match
    |> Seq.sortByDescending(fun regexMatch -> regexMatch.Length)
    // Try to get a value if any exists.
    |> Seq.tryHead
    |> (function
        | None -> 
            (sprintf "Couldn't find regex: %s" regexPattern)
            |> Result.Error
        | Some regexMatch ->
            {   ReadText.ReadText = regexMatch.Value
                ReadText.RemainderText = (if text.Length > regexMatch.Value.Length then text.Substring(regexMatch.Value.Length) else String.Empty) 
            } 
            |> Result.Ok
        )
        