module Step3.Primitives

// Data Type: string -> Result< ReadText, String >

let leftCurlyBracket = Lexer.ReadWord "{"
let rightCurlyBracket = Lexer.ReadWord "}"
let atSymbol = Lexer.ReadWord "@"
let plainTextRegex = Lexer.ReadRegex "[^{]+"
let identifierLabel = Lexer.ReadRegex "[A-Za-z]+"
