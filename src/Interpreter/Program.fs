﻿// Learn more about F# at http://fsharp.org

open System
open FLexer.Core
open Step2.AbstractSyntaxTree

/// Variables shouldn't be hardcoded.
let variables =
    [   "firstName", "Daniel"
        "productCount", "123"
        "productName", "gummi bears"
        "timeDuration", "five minutes"
        "guaranteeDate", "Friday"
    ] |> Map.ofSeq
    
/// "Visit" a tree node.
let rec TreeToString (tree: Tree) =
    match tree with
    | Tree.Root (fragments) -> 
        String.Join("", fragments |> List.map TreeToString)
    | Tree.VariableFragment (variableName) -> 
        variables.Item variableName
    | Tree.TextFragment textFragment -> 
        textFragment

/// Convert a text template to a string
let RunTextTemplate (result: ClassifierBuilderResult<string, Step2.AbstractSyntaxTree.Tree>) =
    match result with
    | Ok(tree, status) -> 
        (TreeToString tree)
    | Error error -> 
        error.ToString()
        


[<EntryPoint>]
let main argv =
    let exampleString = "Hello {@firstName}, only {@productCount} {@productName} left! Order within the next {@timeDuration} to receive before {@guaranteeDate}!"
    
    // If no arguments, display basic example.
    if argv.Length = 0 then
        exampleString
        |> ClassifierStatus<string>.OfString
        |> Step4.CleanParser.ReadTree
        |> RunTextTemplate
        |> printfn "%s"
    else
        argv 
        |> Seq.iter(
            ClassifierStatus<string>.OfString
            >> Step4.CleanParser.ReadTree
            >> RunTextTemplate
            >> printfn "%s"
        )


    0 // return an integer exit code
