module Step4.CleanParser

open FLexer.Core
open FLexer.Core.Tokenizer
open FLexer.Core.Classifiers
open FLexer.Core.Classifier
open Step2.AbstractSyntaxTree
open FLexer.Core.ClassifierBuilder

let leftCurlyBracket = Consumers.TakeChar '{'
let rightCurlyBracket = Consumers.TakeChar '}'
let atSymbol = Consumers.TakeChar '@'
let plainTextRegex = Consumers.TakeRegex "[^{]+"
let identifierLabel = 
    Consumers.TakeRegex "[A-Za-z]+"


let ReadVariable status continuation =
    sub continuation {
        let! status = discard leftCurlyBracket status
        let! status = discard atSymbol status
        let! status = name "variable" identifierLabel status
        let variableName = status.ConsumedText
        let! status = discard rightCurlyBracket status

        return (Tree.VariableFragment variableName), status
    }


let ReadPlainText status continuation =
    sub continuation {
        let! status = name "plainText" plainTextRegex status
        let textFragment = status.ConsumedText

        return (Tree.TextFragment textFragment), status
    }

let ReadTreeFragment status continuation =
    sub continuation {
        let! (fragment, status) = PickOne(status, [ ReadPlainText; ReadVariable ])
        return fragment, status
    }

let ReadTree status =
    root() {
        let! (treeFragments, status) = ZeroOrMore(status, ReadTreeFragment)
        return Tree.Root(List.rev treeFragments), status
    }

