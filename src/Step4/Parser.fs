﻿module Step4.Parser

open System

/// Utility function to remove some Result.bind
let private ResultIgnoreText 
    (mapper: string -> Result<Lexer.ReadText, string>) 
    (value: Result<Lexer.ReadText, string>) =
    value 
    |> Result.bind(fun readText -> mapper readText.RemainderText)
    

/// Everything until curly bracket.
let ReadTextFragment (text: string)
    : Result<Step2.AbstractSyntaxTree.Tree * string, string> =

    text
    |> Step3.Primitives.plainTextRegex
    |> Result.map(fun readText ->
        Step2.AbstractSyntaxTree.Tree.TextFragment(readText.ReadText), 
        readText.RemainderText)

/// {@labelName}
let ReadVariable (text: string)
    : Result<Step2.AbstractSyntaxTree.Tree * string, string> =
    text
    |> Step3.Primitives.leftCurlyBracket
    |> ResultIgnoreText(Step3.Primitives.atSymbol)
    |> Result.bind( fun readText ->
        readText.RemainderText
        |> Step3.Primitives.identifierLabel
        |> Result.bind(fun identiferText ->
            let variableName = 
                identiferText.ReadText
                |> Step2.AbstractSyntaxTree.Tree.VariableFragment
            
            identiferText.RemainderText
            |> Step3.Primitives.rightCurlyBracket
            |> Result.map(fun closingText ->
                variableName, 
                closingText.RemainderText
            )
        )
    )

/// Try variable first, then try the other.
let ReadTreeFragment text
    : Result<Step2.AbstractSyntaxTree.Tree * string, string> =
    match ReadVariable text with
    | Ok _ as success -> success
    | Error _ ->
        match ReadTextFragment text with
        | Ok _ as success -> success
        | Error _ as error -> error
        
/// Recursively read entire tree.
let rec ReadTextTemplate text currentFragments =
    if String.IsNullOrEmpty( text) then
        Ok(Step2.AbstractSyntaxTree.Tree.Root (currentFragments |> List.rev), String.Empty)
    else
        match ReadTreeFragment text with
        | Ok(treeFragment, remainderText) -> ReadTextTemplate remainderText (treeFragment :: currentFragments)
        | Error _ as error -> error

