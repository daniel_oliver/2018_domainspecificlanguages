// Import React
import React from "react";

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Fill,
  Fit,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  CodePane,
  Code,
  Image,
  Text,
  Appear
} from "spectacle";
import CodeSlide from 'spectacle-code-slide';
import Terminal from "spectacle-terminal";

// Import theme
import createTheme from "spectacle/lib/themes/default";
import 'prismjs/components/prism-fsharp';
import preloader from "spectacle/lib/utils/preloader";

// Require CSS
require("prismjs/themes/prism.css");
require("normalize.css");

const theme = createTheme({
  primary: "#091414",
  secondary: "white",
  tertiary: "#0414FF",
  quarternary: "#DEDEDE",
  quintary: "#F0FFFF",
  warning: "#E00F0F"
}, {
    primary: "Montserrat",
    secondary: "Helvetica"
  });

const images = {
  logo: require('../assets/Asset_7.png'),
  none_of_my_business: require('../assets/none_of_my_business.gif'),
  skepticism: require('../assets/skepticism.gif'),
  luke_no: require('../assets/luke_no.gif'),
  much_rejoicing: require('../assets/and_much_rejoicing.gif'),
  flexer_demo: require('../assets/flexer_demo.png'),
  placeholderslide: require('../assets/placeholderslide.jpg')
};

const endTemplate = {
  templateExample: 'Hello {@firstName}, only {@productCount} {@productName} left! Order within the next {@timeDuration} to receive before {@guaranteeDate}!',
  websiteExampleInfo: '@firstName, @productCount, @productName, @timeDuration, @guaranteeDate',
  edi850: require('raw-loader!../assets/EDI_850.txt'),
  ebnfBasic: require('raw-loader!../assets/Step1_Ebnf_Syntax.txt'),
  ebnfSelect: require('raw-loader!../assets/Step1_Select_Ebnf.txt'),
  breakdownIdentifier: "{ @firstName }",
  newLanguageEbnf: require('raw-loader!../assets/Step1_Ebnf_Language.txt'),

  step2AbstractSyntaxTree: require('raw-loader!../src/Step1/AbstractSyntaxTree.fs'),

  step2SqlExample: require('raw-loader!../assets/Step3_Sql_groupby.sql'),

  step3Lexer: require('raw-loader!../src/Common/Lexer.fs'),
  step3Tests: require('raw-loader!../src/Tests/TestLexer.fs'),
  step3Primitives: require('raw-loader!../src/Step3/Primitives.fs'),

  step4Parser: require('raw-loader!../src/Step4/Parser.fs'),
  step4ResultType: require('raw-loader!../assets/fsharp_result.txt'),
  step4ResultTypeBind: require('raw-loader!../assets/fsharp_result_bind.txt'),
  step4ParserTests: require('raw-loader!../src/Tests/TestParser.fs'),

  step4ParserClean: require('raw-loader!../src/Step4.Clean/CleanParser.fs'),

  step5Interpreter: require('raw-loader!../src/Interpreter/Program.fs'),

  step4NoteTextFragment: "Everything until curly bracket {",
  step4NoteVariable: "{@firstName}"
};



preloader(images);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["slide"]} theme={theme} transitionDuration={500} progress="bar">

        <Image src={images.placeholderslide} width="1920" height="1080"></Image>

    {/* #####  START INTRODUCTION  ##### */}
        <Slide bgColor="secondary" textColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="primary">
            Domain Specific Languages
          </Heading>
          <Heading size={3} fit caps lineHeight={1} textColor="primary">
            DSL
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit>
            For Fun and Profit
          </Text>
        </Slide>

        <Slide>
          <Heading size={3} fit textColor="secondary">Daniel Oliver</Heading>
          <Heading size={6} fill textColor="secondary">@a_software_dev</Heading>
          <Image src={images.logo} width="302" height="325"></Image>
        </Slide>

        <Slide>
          <Heading size={6} fill textColor="secondary">What is a Domain Specific Language?</Heading>

          <BlockQuote>
            <Quote textColor="quarternary">DSLs are small languages, focused on a particular aspect of a software system.</Quote>
            <Cite>Martin Fowler</Cite>
          </BlockQuote>
        </Slide>

        <Slide>
          <Heading size={6} textColor="secondary">Domain Specific Languages</Heading>
          <List>
            <Appear>
              <ListItem lineHeight={1} textColor="quarternary">COMPLETELY express a business domain.</ListItem>
            </Appear>
            <Appear>
              <ListItem lineHeight={1} textColor="quarternary">ONLY express a business domain.</ListItem>
            </Appear>
            <Appear>
              <ListItem lineHeight={1} textColor="quarternary">Are usually UNDOCUMENTED messes, which accurately reflects most business domains.</ListItem>
            </Appear>
          </List>
        </Slide>

        <Slide>
          <Image src={images.none_of_my_business} width="500" height="281"></Image>
        </Slide>

        <Slide>
          <Heading size={4} textColor="secondary"> Example DSLs</Heading>
          <List>
            <ListItem>C# LINQ</ListItem>
            <ListItem>EDI 850 - Purchase Order</ListItem>
            <ListItem>Any Javascript view engine: ReactJS, Angular, Vue, etc.</ListItem>
            <ListItem>Salesforce Object Query Language</ListItem>
            <ListItem>Azure Resource Manager (ARM) templates</ListItem>
          </List>
        </Slide>
        
        <Slide>
          <Heading size={3} textColor="secondary">EDI 850?!</Heading>
          <Heading size={5} textColor="secondary">But that's a data format!</Heading>
          <Image src={images.skepticism} width="374" height="280"></Image>
        </Slide>
        
        <Slide>
          <Text textColor="secondary">
            Declarative programming allows us to define the desired result without dictating how to get there.
          </Text>
          <br/>
          <Text textColor="secondary">
            <b>A data format is an interpreted, declarative language.</b>
          </Text>            
        </Slide>
        
        <Slide>
          <Text textColor="secondary">
            EDI 850 is declaring a Purchase Order, but not what to do with it.
          </Text>

          <CodePane
            bgColor="secondary"
            textSize={18}
            source={endTemplate.edi850}/>
        </Slide>
    {/* #####  END INTRODUCTION  ##### */}


    {/* #####  START REASON  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={3}>The Reason and Domain of a DSL</Heading>
          <Heading size={6}>(A contrived example)</Heading>
          <List>
            <ListItem textColor="primary">The Business Case</ListItem>
            <ListItem textColor="primary">The Domain and Data</ListItem>
          </List>
        </Slide>

        <Slide>
          <Heading textColor="secondary" size={3}>The Business Case</Heading>
          <Text textColor="secondary">
            Non-technical "business users" should have an easy, flexible, and secure way to customize text fields on a website.
          </Text>
          <br/>
          <Code bgColor="quarternary">
            Hello Daniel, only 10 giant gummi bears left! Order within the next 42 minutes to receive before Friday!
          </Code>
        </Slide>

        <Slide>
          <Heading textColor="secondary" size={3}>The Domain and Data</Heading>
          <Heading textColor="secondary" size={6}>(Really just data)</Heading>
          <br/>
          <Text textColor="secondary"><b>Business User Input:</b></Text>
          <Code bgColor="quarternary">{ '"' + endTemplate.templateExample + '"' }</Code>
          <br/>
          <br/>
          <Text textColor="secondary"><b>Output:</b> Text</Text>
        </Slide>
    {/* #####  END REASON  ##### */}
        
        <Slide bgColor="quarternary">
          <Heading size={3}>Building a Language</Heading>
          <Heading size={6}>Creating an interpreter</Heading>
          <List>
            <ListItem textColor="primary">Grammar</ListItem>
            <ListItem textColor="primary">Abstract Syntax Tree (AST)</ListItem>
            <ListItem textColor="primary">Lexer</ListItem>
            <ListItem textColor="primary">Parser</ListItem>
            <ListItem textColor="primary">Interpreter</ListItem>
          </List>
        </Slide>
        
    {/* #####  START GRAMMAR  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={3}>Step 1: Grammar</Heading>
          <Heading size={6}>Backus–Naur form</Heading>
          <br/>
          <Text>
            Backus–Naur form (BNF), or some extension like EBNF, is a common notation format to define the rules of a language's grammar.
          </Text>
        </Slide>

        <Slide>
          <Heading size={6} textColor="secondary">Integers and Floats in EBNF</Heading>
          <Text textColor="secondary">Such as: 500, 12.34, 34, 3.14159</Text>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={24}
            source={endTemplate.ebnfBasic}/>
        </Slide>

        <Slide>
          <Heading size={6} textColor="secondary">TSQL Select Statement</Heading>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={20}
            source={endTemplate.ebnfSelect}/>
        </Slide>

        <Slide>
          <Heading size={6} textColor="secondary">New text template language sample</Heading>
          <br/>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={24}
            source={endTemplate.templateExample}/>
        </Slide>

        <Slide>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={21}
            source={endTemplate.templateExample}/>
          <Heading size={6} textColor="secondary">New language breakdown</Heading>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={21}
            source={endTemplate.newLanguageEbnf}/>
        </Slide>

    {/* #####  END GRAMMAR  ##### */}


    {/* #####  START AST  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={4} fill>Step 2: Abstract Syntax Tree</Heading>
          <br/>
          <Text>
            An Abstract Syntax Tree is the data structure that a language is represented by.
          </Text>
          <br/>
          <Text>
            All languages are ultimately a fancy data structure.
          </Text>
        </Slide>
        
        <Slide>
          <Heading size={6} textColor="secondary">Text Template's Abstract Syntax Tree</Heading>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={22}
            source={endTemplate.step2AbstractSyntaxTree}/>
        </Slide>
        
        <Slide>
          <Heading size={4} textColor="secondary">The Challenge</Heading>
          <Text textColor="secondary">
            An Abstract Syntax Tree does not guarantee validity. 
          </Text>
          <br/>

          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={22}
            source={endTemplate.step2SqlExample}/>
          <Heading size={4} textColor="secondary">What's wrong here?</Heading>

          <Appear>
            <Text textColor="secondary">LastName isn't in the GROUP BY.</Text>
          </Appear>
        </Slide>
        
        <Slide>
          <Heading size={4} textColor="secondary">Revised definition</Heading>
          <br/>
          <Text textColor="secondary">
            An Abstract Syntax Tree is a convenient data structure to validate and to transform a language by.
          </Text>
        </Slide>        
    {/* #####  END AST  ##### */}


        <Slide bgColor="quarternary">
          <Heading size={6}>Two Steps Covered so far:</Heading>
          <List>
            <ListItem textColor="primary"><b>Grammar</b></ListItem>
            <ListItem textColor="primary"><b>Abstract Syntax Tree (AST)</b></ListItem>
          </List>
          <List>
            <ListItem textColor="primary">Lexer</ListItem>
            <ListItem textColor="primary">Parser</ListItem>
            <ListItem textColor="primary">Interpreter</ListItem>
          </List>
        </Slide>


    {/* #####  START Lexer & Parser  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={4} fill>Step 3 and 4: Lexer &amp; Parser</Heading>
          <br/>
          <Text>
            A Lexer performs "Lexical Analysis": splitting strings into words and attaching meaning.
          </Text>
          <br/>
          <Text>
            A Parser performs "Syntax Analysis": a grammar checker.
          </Text>
        </Slide>

        <Slide>
          <Heading size={4} textColor="secondary">Parser Generators</Heading>
          <br/>
          <Text textColor="secondary">
            Many Parser Generators exist to offer a convenient method of Lexing and Parsing, by generating the code to do this from the Grammar in EBNF notation.
          </Text>
          <List>
            <ListItem textColor="secondary">YACC/Lex</ListItem>
            <ListItem textColor="secondary">ANTLR</ListItem>
            <ListItem textColor="secondary">GNU bison</ListItem>
          </List>
        </Slide>
        
        <Slide>
          <Heading size={4} textColor="secondary">Recursive Descent Parser</Heading>
          <br/>
          <Text textColor="secondary">
            Traditionally, Lexing and Parsing have been separate steps, but some approaches combine the two, and I'm going to show you one: Recursive Descent Parsing.
          </Text>
        </Slide>
    {/* #####  END Lexer & Parser  ##### */}
        

    {/* #####  START Lexer  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={4} fill>Step 3: Lexer</Heading>
          <br/>
          <Text>
            Even though Recursive Descent Parser combines the Lexing and Parsing steps, let's build them separately.
          </Text>
        </Slide>

        <Slide>
          <Heading size={4} textColor="secondary">What the Lexer needs</Heading>
          <br/>
          <Text textColor="secondary">
            The Lexer needs to be able to read Strings and Regex Patterns at the beginning of the input string.            
          </Text>
          <br/>
          <Text textColor="secondary">Input String:</Text>
          <Code bgColor="quarternary">{ endTemplate.templateExample }</Code>
        </Slide>  

        <CodeSlide
          lang="xml"
          bgColor="quarternary"
          code={endTemplate.step3Lexer}
          ranges={[
            { loc: [4, 8], note: "Each Lexer Function's output" },
            { loc: [9, 25], note: "Read a simple word" },
            { loc: [26, 38], note: "Read a regex match" }
          ]}/>

        <CodeSlide
          lang="xml"
          bgColor="quarternary"
          code={endTemplate.step3Tests}
          ranges={[
            { loc: [7, 18], note: "Test reading word function succeeds" },
            { loc: [19, 30], note: "Test reading word function fails" }
          ]}/>

        <Slide>
          <Heading size={4} textColor="secondary">Remember this?</Heading>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={21}
            source={endTemplate.newLanguageEbnf}/>
        </Slide>
        
        <Slide>
          <Text textColor="secondary">Here is the primitive applied functions.</Text>
          <CodePane
            bgColor="secondary"
            lang="xml"
            textSize={22}
            source={endTemplate.step3Primitives}/>
        </Slide>
        
        <Slide>
          <Text textColor="secondary">Notice that half the grammar is missing?</Text>
          <br/>
          <Text textColor="secondary">The rest of the grammar depends upon other grammar's definition.</Text>
          <br/>
          <Text textColor="secondary">These base words and symbols are called a "lexeme".</Text>
        </Slide>
    {/* #####  END Lexer  ##### */}


    {/* #####  START Parser  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={4} fill>Step 4: Parser</Heading>
          <br/>
          <Text>
            The Recursive Descent Parser: Filling out the Abstract Syntax Tree.
          </Text>
        </Slide>

        <Slide>
          <Heading textColor="secondary" size={4} fill>Result Type</Heading>
          <Text textColor="secondary">
            Before I dive into building the parser, I want to touch on the standard Result type:
          </Text>
          <CodePane textSize={24} bgColor="quarternary" source={ endTemplate.step4ResultType } />
          <br/>
          <Text textColor="secondary">          
            Every piece of the parser will return either "Ok" or "Error" to express the result of an action.
          </Text>
        </Slide>

        <Slide>
          <Heading textColor="secondary" size={4} fill>Result Type bind</Heading>
          <Text textColor="secondary">
            Result Types have this operator "bind" that executes a given function if the input is "Ok", and skips if "Error".
          </Text>
          <CodePane textSize={24} bgColor="quarternary" source={ endTemplate.step4ResultTypeBind } />
        </Slide>

        <CodeSlide
          lang="fsharp"
          bgColor="quarternary"
          code={endTemplate.step4Parser}
          ranges={[
            { loc: [12, 21], note: endTemplate.step4NoteTextFragment },
            { loc: [22, 40], note: endTemplate.step4NoteVariable },
            { loc: [45, 54], note: "Tries Variable first, and then fragment" },
            { loc: [55, 63], note: "Recursive function to create list from fragments" },
            { loc: [4, 10], note: "Utility Function to smooth out Result.bind" }
          ]}/>

        <Slide>
          <Heading textColor="secondary" size={3}>Recursive Descent Parser</Heading>
          <br/>
          <Text textColor="secondary">
            Each lexeme from the lexer is attempted one at a time and chained together with Result.bind and Result.map
            <br/>
            <br/>
            If the Result is Ok, try the next one. 
            <br/>
            <br/>
            If any of the Results are an Error, stop there.
          </Text>
        </Slide>
        
        <CodeSlide
          lang="fsharp"
          bgColor="quarternary"
          code={endTemplate.step4ParserTests}
          ranges={[
            { loc: [11, 25], note: "Testing text fragment function" },
            { loc: [26, 40], note: "Testing variable fragment function" }
          ]}/>

        <Slide>
          <Heading size={4} textColor="secondary">The hard part is over</Heading>
          <br/>
          <Text textColor="secondary">Now that you've seen what's happening, let's use an external library to clean up that code readability, but does the exact same thing.</Text>
          <br/>
          <Text textColor="secondary">Disclaimer: I wrote the library</Text>
          <br/>
          <Text textColor="secondary">(FLexer on NuGet)</Text>
        </Slide>  

        <CodeSlide
          lang="fsharp"
          bgColor="quarternary"
          code={endTemplate.step4ParserClean}
          ranges={[
            { loc: [2, 15], note: "Primitive functions" },
            { loc: [17, 27], note: "Read a variable" },
            { loc: [29,36], note: "Read plain text" },
            { loc: [37, 42], note: "Read a variable or plain text" },
            { loc: [43, 48], note: "Read all the fragments into a list" }
          ]}/>

        <Slide>
          <Heading size={4} textColor="secondary">Quick Reminder</Heading>
          <br/>
          <Text textColor="secondary">Input String:</Text>
          <CodePane textSize={26} bgColor="quarternary" source={ endTemplate.templateExample } />
        </Slide>  

        <Slide>
          <Heading size={4} fit textColor="secondary">Demo</Heading>
          <Image src={images.flexer_demo} width="1000" height="579"></Image>
        </Slide>
    {/* #####  END Parser  ##### */}


        <Slide bgColor="quarternary">
          <Heading size={6}>Four Steps Covered so far:</Heading>
          <List>
            <ListItem textColor="primary"><b>Grammar</b></ListItem>
            <ListItem textColor="primary"><b>Abstract Syntax Tree (AST)</b></ListItem>
            <ListItem textColor="primary"><b>Lexer</b></ListItem>
            <ListItem textColor="primary"><b>Parser</b></ListItem>
          </List>
          <List>
            <ListItem textColor="primary">Interpreter</ListItem>
          </List>
        </Slide>

    
    {/* #####  START INTERPRETER  ##### */}
        <Slide bgColor="quarternary">
          <Heading size={3}>Step 5: Interpreter</Heading>
          <br/>
          <List>
            <ListItem textColor="primary"><b>Read raw language</b></ListItem>
            <ListItem textColor="primary"><b>Generate Abstract Syntax Tree (AST)</b></ListItem>
            <ListItem textColor="primary"><b>Validate and Transform AST</b></ListItem>
            <ListItem textColor="primary"><b>Output or perform Result</b></ListItem>
          </List>
        </Slide>

        
        <CodeSlide
          lang="fsharp"
          bgColor="quarternary"
          code={endTemplate.step5Interpreter}
          ranges={[
            { loc: [6, 14], note: "Variables shouldn't be hardcoded" },
            { loc: [35, 46], note: "Use a default basic example" },
            { loc: [46, 55], note: "Use all parameter strings as examples" },
            { loc: [25,32], note: "Transform AST to result" },
            { loc: [15, 24], note: "Recursive function to transform AST" }
          ]}/>
        
          <Slide>
            <Heading size={4} textColor="secondary">Default hardcoded example</Heading>
            <br/>
            <Terminal title="Windows Powershell" output={[
                "> dotnet run",
                <div style={{ color: "#33F969"}}>
                  <div>Hello Daniel, only 123 gummi bears left! Order within the next five minutes</div>
                  <div>to receive before Friday!</div>
                </div>]}
              />
          </Slide>  
          
          <Slide>
            <Heading size={4} textColor="secondary">Transforming arguments</Heading>
            <br/>
            <Terminal title="Windows Powershell" output={[
                "> dotnet run \"Hello, {@firstName}, good evening!\" \"{@productName} are great!\"",
                <div style={{ color: "#33F969"}}>
                  <div>Hello, Daniel, good evening!</div>
                  <div>gummi bears are great!</div>
                </div>]}
              />
          </Slide>  
          
        <Slide>
          <Image src={images.much_rejoicing} fill width="740" height="560"></Image>
        </Slide>
    {/* #####  END INTERPRETER  ##### */}

        <Slide bgColor="quarternary">
          <Heading size={5}>What I haven't talked about</Heading>
          <List>
            <ListItem textColor="primary"><b>Type Systems</b></ListItem>
            <ListItem textColor="primary"><b>Multiple Source File Includes</b></ListItem>
            <ListItem textColor="primary"><b>Production Usage</b></ListItem>
            <ListItem textColor="primary"><b>AND so much more!</b></ListItem>
          </List>
        </Slide>

        <Image src={images.placeholderslide} width="1920" height="1080"></Image>

      </Deck>
    );
  }
}
