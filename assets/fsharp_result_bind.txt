let item1 = Ok "someValue"
let binding_example =
    item1
    |> Result.bind(fun text ->
        printfn "%s text"
        // "someValue"
        Error ("someArbitraryValue")
    )
printfn "%A" binding_example
// "Error someArbitraryValue"