SELECT FirstName
      ,LastName
      ,MAX(BirthDate) [MaxBirthDate]
FROM Customers
GROUP BY FirstName
